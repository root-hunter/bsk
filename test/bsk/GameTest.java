package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;


public class GameTest {

	@Test
	public void testAdd10Frame(){
		
		try {
			Game game = new Game();
			
			Frame frame1 = new Frame(3, 2);
			game.addFrame(frame1);

			Frame frame2 = new Frame(3, 0);
			game.addFrame(frame2);

			Frame frame3 = new Frame(1, 0);
			game.addFrame(frame3);

			Frame frame4 = new Frame(5, 5);
			game.addFrame(frame4);

			Frame frame5 = new Frame(2, 1);
			game.addFrame(frame5);

			Frame frame6 = new Frame(2, 2);
			game.addFrame(frame6);

			Frame frame7 = new Frame(3, 2);
			game.addFrame(frame7);

			Frame frame8 = new Frame(3, 2);
			game.addFrame(frame8);

			Frame frame9 = new Frame(3, 2);
			game.addFrame(frame9);

			Frame frame10 = new Frame(3, 2);
			game.addFrame(frame10);
			
		}catch (BowlingException e) {
			fail();
		}	
	}
	
	@Test
	public void testGetFrameAt() throws BowlingException {
		Game game = new Game();
		
		Frame frame0 = new Frame(3, 2);
		game.addFrame(frame0);

		Frame frame1 = new Frame(3, 0);
		game.addFrame(frame1);

		Frame frame2 = new Frame(1, 0);
		game.addFrame(frame2);

		Frame frame3 = new Frame(5, 5);
		game.addFrame(frame3);

		Frame frame4 = new Frame(2, 1);
		game.addFrame(frame4);

		Frame frame5 = new Frame(2, 2);
		game.addFrame(frame5);

		Frame frame6 = new Frame(3, 2);
		game.addFrame(frame6);

		Frame frame7 = new Frame(3, 2);
		game.addFrame(frame7);

		Frame frame8 = new Frame(3, 2);
		game.addFrame(frame8);

		Frame frame9 = new Frame(3, 2);
		game.addFrame(frame9);		
		
		
		assertTrue(game.getFrameAt(4).equals(frame4));
	}
	
	@Test
	public void testGetGameScore() throws BowlingException {
		Game game = new Game();
		
		Frame frame0 = new Frame(1, 5);
		game.addFrame(frame0);

		Frame frame1 = new Frame(3, 6);
		game.addFrame(frame1);

		Frame frame2 = new Frame(7, 2);
		game.addFrame(frame2);

		Frame frame3 = new Frame(3, 6);
		game.addFrame(frame3);

		Frame frame4 = new Frame(4, 4);
		game.addFrame(frame4);

		Frame frame5 = new Frame(5, 3);
		game.addFrame(frame5);

		Frame frame6 = new Frame(3, 3);
		game.addFrame(frame6);

		Frame frame7 = new Frame(4, 5);
		game.addFrame(frame7);

		Frame frame8 = new Frame(8, 1);
		game.addFrame(frame8);

		Frame frame9 = new Frame(2, 6);
		game.addFrame(frame9);		
		
		
		assertEquals(81, game.calculateScore());		
	}
	
	@Test
	public void testGetScoreWithSpareFrame() throws BowlingException {
		Game game = new Game();
		
		Frame frame0 = new Frame(1, 9);
		game.addFrame(frame0);

		Frame frame1 = new Frame(3, 6);
		game.addFrame(frame1);

		Frame frame2 = new Frame(7, 2);
		game.addFrame(frame2);

		Frame frame3 = new Frame(3, 6);
		game.addFrame(frame3);

		Frame frame4 = new Frame(4, 4);
		game.addFrame(frame4);

		Frame frame5 = new Frame(5, 3);
		game.addFrame(frame5);

		Frame frame6 = new Frame(3, 3);
		game.addFrame(frame6);

		Frame frame7 = new Frame(4, 5);
		game.addFrame(frame7);

		Frame frame8 = new Frame(8, 1);
		game.addFrame(frame8);

		Frame frame9 = new Frame(2, 6);
		game.addFrame(frame9);		
		
		
		assertEquals(88, game.calculateScore());		
	}
	
	@Test
	public void testGetScoreWithStrikeFrame() throws BowlingException {
		Game game = new Game();
		
		Frame frame0 = new Frame(10, 0);
		game.addFrame(frame0);

		Frame frame1 = new Frame(3, 6);
		game.addFrame(frame1);

		Frame frame2 = new Frame(7, 2);
		game.addFrame(frame2);

		Frame frame3 = new Frame(3, 6);
		game.addFrame(frame3);

		Frame frame4 = new Frame(4, 4);
		game.addFrame(frame4);

		Frame frame5 = new Frame(5, 3);
		game.addFrame(frame5);

		Frame frame6 = new Frame(3, 3);
		game.addFrame(frame6);

		Frame frame7 = new Frame(4, 5);
		game.addFrame(frame7);

		Frame frame8 = new Frame(8, 1);
		game.addFrame(frame8);

		Frame frame9 = new Frame(2, 6);
		game.addFrame(frame9);		
		
		
		assertEquals(94, game.calculateScore());		
	}
	
	@Test
	public void testGetScoreWithStrikeFrameFoolowedBySpareFrame() throws BowlingException {
		Game game = new Game();
		
		Frame frame0 = new Frame(10, 0);
		game.addFrame(frame0);

		Frame frame1 = new Frame(4, 6);
		game.addFrame(frame1);

		Frame frame2 = new Frame(7, 2);
		game.addFrame(frame2);

		Frame frame3 = new Frame(3, 6);
		game.addFrame(frame3);

		Frame frame4 = new Frame(4, 4);
		game.addFrame(frame4);

		Frame frame5 = new Frame(5, 3);
		game.addFrame(frame5);

		Frame frame6 = new Frame(3, 3);
		game.addFrame(frame6);

		Frame frame7 = new Frame(4, 5);
		game.addFrame(frame7);

		Frame frame8 = new Frame(8, 1);
		game.addFrame(frame8);

		Frame frame9 = new Frame(2, 6);
		game.addFrame(frame9);		
		
		
		assertEquals(103, game.calculateScore());		
	}
	
	@Test
	public void testGetScoreWithStrikeFrameFoolowedByStrikeFrame() throws BowlingException {
		Game game = new Game();
		
		Frame frame0 = new Frame(10, 0);
		game.addFrame(frame0);

		Frame frame1 = new Frame(10, 0);
		game.addFrame(frame1);

		Frame frame2 = new Frame(7, 2);
		game.addFrame(frame2);

		Frame frame3 = new Frame(3, 6);
		game.addFrame(frame3);

		Frame frame4 = new Frame(4, 4);
		game.addFrame(frame4);

		Frame frame5 = new Frame(5, 3);
		game.addFrame(frame5);

		Frame frame6 = new Frame(3, 3);
		game.addFrame(frame6);

		Frame frame7 = new Frame(4, 5);
		game.addFrame(frame7);

		Frame frame8 = new Frame(8, 1);
		game.addFrame(frame8);

		Frame frame9 = new Frame(2, 6);
		game.addFrame(frame9);		
		
		assertEquals(112, game.calculateScore());		
	}
	
	@Test
	public void testGetScoreWithSpareFrameFoolowedBySpareFrame() throws BowlingException {
		Game game = new Game();
		
		Frame frame0 = new Frame(8, 2);
		game.addFrame(frame0);

		Frame frame1 = new Frame(5, 5);
		game.addFrame(frame1);

		Frame frame2 = new Frame(7, 2);
		game.addFrame(frame2);

		Frame frame3 = new Frame(3, 6);
		game.addFrame(frame3);

		Frame frame4 = new Frame(4, 4);
		game.addFrame(frame4);

		Frame frame5 = new Frame(5, 3);
		game.addFrame(frame5);

		Frame frame6 = new Frame(3, 3);
		game.addFrame(frame6);

		Frame frame7 = new Frame(4, 5);
		game.addFrame(frame7);

		Frame frame8 = new Frame(8, 1);
		game.addFrame(frame8);

		Frame frame9 = new Frame(2, 6);
		game.addFrame(frame9);		
		
		assertEquals(98, game.calculateScore());		
	}
	
	@Test
	public void testSetAndGetFirstBonusThrow() throws BowlingException {
		Game game = new Game();
		
		game.setFirstBonusThrow(6);
		
		assertEquals(6, game.getFirstBonusThrow());		
	}
	
	@Test
	public void testGetScoreIfLastFrameIsSpare() throws BowlingException {
		Game game = new Game();
		
		Frame frame0 = new Frame(1, 5);
		game.addFrame(frame0);

		Frame frame1 = new Frame(3, 6);
		game.addFrame(frame1);

		Frame frame2 = new Frame(7, 2);
		game.addFrame(frame2);

		Frame frame3 = new Frame(3, 6);
		game.addFrame(frame3);

		Frame frame4 = new Frame(4, 4);
		game.addFrame(frame4);

		Frame frame5 = new Frame(5, 3);
		game.addFrame(frame5);

		Frame frame6 = new Frame(3, 3);
		game.addFrame(frame6);

		Frame frame7 = new Frame(4, 5);
		game.addFrame(frame7);

		Frame frame8 = new Frame(8, 1);
		game.addFrame(frame8);

		Frame frame9 = new Frame(2, 8);
		game.addFrame(frame9);		
		
		game.setFirstBonusThrow(7);
		
		assertEquals(90, game.calculateScore());		
	}
	
	@Test
	public void testSetAndGetSecondBonusThrow() throws BowlingException {
		Game game = new Game();
		
		game.setSecondBonusThrow(6);
		
		assertEquals(6, game.getSecondBonusThrow());		
	}
	
	@Test
	public void testGetScoreIfLastFrameIsStrike() throws BowlingException {
	Game game = new Game();
		
		Frame frame0 = new Frame(1, 5);
		game.addFrame(frame0);

		Frame frame1 = new Frame(3, 6);
		game.addFrame(frame1);

		Frame frame2 = new Frame(7, 2);
		game.addFrame(frame2);

		Frame frame3 = new Frame(3, 6);
		game.addFrame(frame3);

		Frame frame4 = new Frame(4, 4);
		game.addFrame(frame4);

		Frame frame5 = new Frame(5, 3);
		game.addFrame(frame5);

		Frame frame6 = new Frame(3, 3);
		game.addFrame(frame6);

		Frame frame7 = new Frame(4, 5);
		game.addFrame(frame7);

		Frame frame8 = new Frame(8, 1);
		game.addFrame(frame8);

		Frame frame9 = new Frame(10, 0);
		game.addFrame(frame9);		
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);

		
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testGetScoreOfPerfectGame() throws BowlingException {
	Game game = new Game();
		
		Frame frame0 = new Frame(10, 0);
		game.addFrame(frame0);

		Frame frame1 = new Frame(10, 0);
		game.addFrame(frame1);

		Frame frame2 = new Frame(0, 10);
		game.addFrame(frame2);

		Frame frame3 = new Frame(10, 0);
		game.addFrame(frame3);

		Frame frame4 = new Frame(0, 10);
		game.addFrame(frame4);

		Frame frame5 = new Frame(10, 0);
		game.addFrame(frame5);

		Frame frame6 = new Frame(10, 0);
		game.addFrame(frame6);

		Frame frame7 = new Frame(10, 0);
		game.addFrame(frame7);

		Frame frame8 = new Frame(10, 0);
		game.addFrame(frame8);

		Frame frame9 = new Frame(10, 0);
		game.addFrame(frame9);		
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);

		
		assertEquals(300, game.calculateScore());
	}

}
