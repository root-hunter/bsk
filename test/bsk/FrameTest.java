package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testGetFirstThrow() throws BowlingException {
		int firstThrow = 2;
		int secondThrow = 5;
		
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		
		assertEquals(firstThrow, frame.getFirstThrow());
	}
	
	@Test
	public void testGetSecondThrow() throws BowlingException {
		int firstThrow = 2;
		int secondThrow = 5;
		
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		
		assertEquals(secondThrow, frame.getSecondThrow());
	}
	
	@Test
	public void testGetFrameScore() throws BowlingException {
		int firstThrow = 3;
		int secondThrow = 5;
		
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		
		assertEquals(firstThrow + secondThrow, frame.getScore());
	}
	
	@Test
	public void testGetAndSetBonus() throws BowlingException {
		Frame frame = new Frame(2, 8);
		int inputBonus = 2;
		
		frame.setBonus(inputBonus);
		
		assertEquals(inputBonus, frame.getBonus());
	}
	
	@Test
	public void testFrameShouldBeSpare() throws BowlingException {
		int firstThrow = 1;
		int secondThrow = 9;
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		assertTrue(frame.isSpare());
		
	}
	
	@Test
	public void testFrameShouldNotBeSpare() throws BowlingException {
		int firstThrow = 2;
		int secondThrow = 5;
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		assertFalse(frame.isSpare());
		
	}
	
	@Test
	public void testGetScoreWithBonus() throws BowlingException {
		int firstThrow = 5;
		int secondThrow = 5;
		
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		frame.setBonus(5);
		
		
		assertEquals(firstThrow + secondThrow + 5, frame.getScore());
	}
	
	@Test
	public void testFrameSholdBeStrike() throws BowlingException {
		int firstThrow = 10;
		int secondThrow = 0;
		
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void testGetScoreWithStrike() throws BowlingException {
		int firstThrow = 10;
		int secondThrow = 0;
		
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		assertEquals(10, frame.getScore());
	}
	

}
