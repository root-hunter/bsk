package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	private int firstBonusThrow;
	private int secondBonusThrow;

	private ArrayList<Frame> frameList = null;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		firstBonusThrow = 0;
		secondBonusThrow = 0;
		frameList = new ArrayList<>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		frameList.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return frameList.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		
		if(isPerfectGame()) {
			score = 300;
		}else {
			score = getScore(score);
		}
		return score;	
	}
	
	private boolean isPerfectGame() {
		for(Frame frame : frameList) {
			if(!frame.isStrike()) return false;
		}
		
		return (firstBonusThrow == 10 && secondBonusThrow == 10);
	}
	
	private boolean isLastFrame(int i) {
		return i == frameList.size() - 1;
	}

	private int getScore(int score) {
		for(int i = 0; i < frameList.size(); ++i) {
			Frame frame = frameList.get(i);
			
			if(isLastFrame(i) && frame.isSpare()) {
				score += frame.getScore();
				score += firstBonusThrow;
				
			}else if(isLastFrame(i) && frame.isStrike()){
				score += frame.getScore();
				score += firstBonusThrow;
				score += secondBonusThrow;
			}else{
				if(frame.isSpare()) {
					frame.setBonus(frameList.get(i + 1).getFirstThrow());
					
				}else if(frame.isStrike()) {
					frame.setBonus(
							(frameList.get(i + 1).isStrike()) 
								? frameList.get(i + 1).getScore() + frameList.get(i + 2).getFirstThrow()
									: frameList.get(i + 1).getScore());
				}
				
				score += frame.getScore();
			}
		}
		return score;
	}
}
